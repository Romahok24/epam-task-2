package by.epam;

import java.sql.*;
import java.util.MissingResourceException;
import java.util.Properties;

public class Connector {
    private static Connector instance;
    private Connection connection;

    public Connector(){
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            ConfigurationManager manager = ConfigurationManager.INSTANCE;
            String url = manager.getConfig("url");
            String user = manager.getConfig("user");
            String pass = manager.getConfig("password");
            Properties prop = new Properties();
            prop.put("user", user);
            prop.put("password", pass);
            connection = DriverManager.getConnection(url, prop);
        }catch(MissingResourceException e) {
            System.err.println("properties file is missing " + e);
        }catch(SQLException e) {
            System.err.println("not obtained connection " + e);
        }catch (ClassNotFoundException e){
        }
    }

    public static Connector getInstance(){
        if(instance == null){
            instance = new Connector();
        }
        return instance;
    }

    public Connection getConnection(){
        if(connection != null){
            return connection;
        }
        return null;
    }

    public Statement getStatement() throws SQLException {
        if(connection != null) {
            Statement statement = connection.createStatement();

            if(statement != null) {
                return statement;
            }
        }
        throw new SQLException("connection or statement is null");
    }

    public void closeStatement(Statement statement) {
        if(statement != null) {
            try{
                statement.close();
            }catch(SQLException e) {
                System.err.println("statement is null " + e);
            }
        }
    }

    public void closeConnection() {
        if(connection != null) {
            try{
                connection.close();
            }catch (SQLException e) {
                System.err.println(" wrong connection" + e);
            }
        }
    }

    public PreparedStatement getPreparedStatement(String sql){
        try {
            if (connection != null) {
                PreparedStatement statement = connection.prepareStatement(sql);

                if (statement != null) {
                    return statement;
                }
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }
}
