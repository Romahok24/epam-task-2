package by.epam.subject;

import java.util.ArrayList;
import java.util.List;

public class Supplier extends Entity {
    private String name;
    private List<ProductInBakery> productsInBakery;
    private List<Product> products;
    private List<SoldProduct> soldProducts;
    private String password;

    public Supplier(){
        products = new ArrayList<>();
        productsInBakery = new ArrayList<>();
        soldProducts = new ArrayList<>();
    }

    public Supplier(int id, String name, String password){
        this.setId(id);
        this.name = name;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public void addProduct(Product product){
        products.add(product);
    }

    public void addProductInBakery(ProductInBakery product){
        productsInBakery.add(product);
    }
    public void addSoldProduct(SoldProduct soldProduct){
        soldProducts.add(soldProduct);
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "Supplier{" +
                "name='" + name + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}