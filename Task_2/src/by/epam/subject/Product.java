package by.epam.subject;

public class Product extends Entity {
    protected String name;
    protected int supplierId;
    protected Supplier supplier;
    protected String type;
    protected double price;

    public Product(){}

    public Product(int id, String name, Supplier supplier, String type, double price){
        this.setId(id);
        this.name = name;
        this.supplier = supplier;
        this.supplierId = supplier.getId();
        this.type = type;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(int supplierId) {
        this.supplierId = supplierId;
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", supplierId=" + supplierId +
                ", supplier=" + supplier.getName() +
                ", type='" + type + '\'' +
                '}';
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
