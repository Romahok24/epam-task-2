package by.epam.subject;

import java.util.Date;

public class SoldProduct extends ProductInBakery {
    private Date dateOfSale;
    private int cashBoxId;

    public SoldProduct(){}

    public SoldProduct(int id, String name, Supplier supplier, Date date, String type, double price){
        super(id, name, supplier, date, type, price);
       dateOfSale = new java.sql.Date(System.currentTimeMillis());
    }

    public Date getDateOfSale() {
        return dateOfSale;
    }

    public void setDateOfSale(Date dateOfSale) {
        this.dateOfSale = dateOfSale;
    }

    public int getCashBoxId() {
        return cashBoxId;
    }

    public void setCashBoxId(int cashBoxId) {
        this.cashBoxId = cashBoxId;
    }

    @Override
    public String toString() {
        return "SoldProduct{" +
                "dateOfSale=" + dateOfSale +
                ", cashBoxId=" + cashBoxId +
                ", name='" + name + '\'' +
                ", supplierId=" + supplierId +
                ", supplier=" + supplier.getName() +
                ", type='" + type + '\'' +
                ", price=" + price +
                '}';
    }
}
