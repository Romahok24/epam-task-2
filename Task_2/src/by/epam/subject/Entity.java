package by.epam.subject;

import java.io.Serializable;

public class Entity implements Serializable, Comparable<Entity> {
    private int id;

    public Entity(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int compareTo(Entity o) {
        return (this.id - o.getId());
    }
}
