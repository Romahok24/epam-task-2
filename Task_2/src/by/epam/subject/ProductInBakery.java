package by.epam.subject;

import java.util.Date;

public class ProductInBakery extends Product {
    protected Date deliverDate;
    private final double markup = 1.2;

    public ProductInBakery(){}

    public ProductInBakery(int id, String name, Supplier supplier, String type, double price){
        super(id, name, supplier, type, price );
        deliverDate = new java.sql.Date(System.currentTimeMillis());
        this.price *= markup;
    }

    public ProductInBakery(int id, String name, Supplier supplier, Date date, String type, double price){
        super(id, name, supplier, type, price);
        deliverDate = date;
    }

    public Date getDeliverDate() {
        return deliverDate;
    }

    public void setDeliverDate(Date deliverDate) {
        this.deliverDate = deliverDate;
    }

    @Override
    public String toString() {
        return "ProductInBakery{" +
                "id=" + getId() +
                ", name='" + name + '\'' +
                ", supplierId=" + supplierId +
                ", supplier=" + supplier.getName() +
                ", type='" + type + '\'' +
                ", price=" + price +
                '}';
    }
}
