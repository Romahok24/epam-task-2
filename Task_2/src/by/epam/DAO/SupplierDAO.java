package by.epam.DAO;

import by.epam.ConfigurationManager;
import by.epam.Connector;
import by.epam.subject.Supplier;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class SupplierDAO extends AbstractDAO<Supplier> {
    ConfigurationManager manager = ConfigurationManager.INSTANCE;

    public SupplierDAO(){
        this.connector = Connector.getInstance();
    }

    @Override
    public boolean create(Supplier supplier) {
        PreparedStatement st = null;

        try{
            st = connector.getPreparedStatement(manager.getSqlQueries("addSupplier"));

            st.setString(1, Integer.toString(supplier.getId()));
            st.setString(2, supplier.getName());
            st.setString(3, supplier.getPassword());

            st.executeUpdate();

            return true;
        } catch (SQLException e){
            e.printStackTrace();
        }finally {
            connector.closeStatement(st);
        }

        return false;
    }

    @Override
    public List<Supplier> read() {
        List<Supplier> suppliers = new ArrayList<>();

        Statement st = null;

        try{
            st = connector.getStatement();
            ResultSet rs = st.executeQuery(manager.getSqlQueries("readSuppliers"));

            while(rs.next()){
                Supplier supplier = new Supplier();
                supplier.setId(rs.getInt("id"));
                supplier.setName(rs.getString("name"));
                supplier.setPassword(rs.getString("password"));

                suppliers.add(supplier);
            }

        }catch (SQLException e){

        } finally {
            this.closeStatement(st);
        }

        return suppliers;
    }

    @Override
    public boolean update(Supplier entity) {
        PreparedStatement st = null;

        try{
            st = connector.getPreparedStatement(manager.getSqlQueries("updateSupplier"));

            st.setString(1, entity.getName());
            st.setString(2, entity.getPassword());
            st.setString(3, Integer.toString(entity.getId()));

            st.executeUpdate();

            return true;
        } catch (SQLException e){
            e.printStackTrace();
        } finally {
            connector.closeStatement(st);
        }


        return false;
    }

    @Override
    public boolean delete(int id) {
        PreparedStatement st = null;

        try{
            st = connector.getPreparedStatement(manager.getSqlQueries("deleteSupplier"));

            st.setString(1, Integer.toString(id));
            st.executeUpdate();

            return true;
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            connector.closeStatement(st);
        }

        return false;
    }
}
