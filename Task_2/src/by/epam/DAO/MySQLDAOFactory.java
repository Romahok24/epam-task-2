package by.epam.DAO;

public class MySQLDAOFactory extends DAOFactory {
    @Override
    public ProductDAO getProductDAO() {
        return new ProductDAO();
    }

    @Override
    public SupplierDAO getSupplierDAO() {
        return new SupplierDAO();
    }

    @Override
    public SoldProductDAO getSoldProductDAO() {
        return new SoldProductDAO();
    }

    @Override
    public ProductInBakeryDAO getProductInBakeryDAO() {
        return new ProductInBakeryDAO();
    }
}
