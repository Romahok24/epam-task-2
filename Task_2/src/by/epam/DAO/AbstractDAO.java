package by.epam.DAO;

import by.epam.Connector;
import by.epam.subject.Entity;

import java.sql.Statement;
import java.util.List;

public abstract class AbstractDAO<T extends Entity> {
    protected Connector connector;

    public abstract boolean create(T entity);
    public abstract List<T> read();
    public abstract boolean update(T entity);
    public abstract boolean delete(int id);

    public void closeConnection(){
        connector.closeConnection();
    }

    public void closeStatement(Statement statement){
        connector.closeStatement(statement);
    }
}
