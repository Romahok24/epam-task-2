package by.epam.DAO;

import by.epam.ConfigurationManager;
import by.epam.Connector;
import by.epam.subject.Product;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ProductDAO extends AbstractDAO<Product> {
    ConfigurationManager manager = ConfigurationManager.INSTANCE;

    public ProductDAO(){
        this.connector = Connector.getInstance();
    }

    @Override
    public boolean create(Product product) {
        PreparedStatement st = null;

        try {
            st = connector.getPreparedStatement(manager.getSqlQueries("addProduct"));

            st.setString(1, Integer.toString(product.getId()));
            st.setString(2, product.getName());
            st.setString(3, Integer.toString(product.getSupplierId()));
            st.setString(4, product.getType());
            st.setString(5, Double.toString(product.getPrice()));

            st.executeUpdate();

            return true;
        } catch (SQLException e){
            e.printStackTrace();
        } finally {
            connector.closeStatement(st);
        }

        return false;
    }

    @Override
    public List<Product> read() {
        List<Product> products = new ArrayList<>();
        Statement st = null;

        try {
            st = connector.getStatement();
            ResultSet rs = st.executeQuery(manager.getSqlQueries("readProducts"));

            while (rs.next()){
                Product product = new Product();
                product.setId(rs.getInt("id"));
                product.setName(rs.getString("name"));
                product.setSupplierId(rs.getInt("suppliersId"));
                product.setType(rs.getString("type"));
                product.setPrice(rs.getDouble("price"));

                products.add(product);
            }
        } catch (SQLException e){
            e.printStackTrace();
        }finally {
            this.closeStatement(st);
        }
        return products;
    }

    @Override
    public boolean update(Product entity) {
        PreparedStatement st = null;

        try{
            st = connector.getPreparedStatement(manager.getSqlQueries("updateProduct"));

            st.setString(1, entity.getName());
            st.setString(2, Integer.toString(entity.getSupplierId()));
            st.setString(3, entity.getType());
            st.setString(4, Double.toString(entity.getPrice()));
            st.setString(5, Integer.toString(entity.getId()));

            st.executeUpdate();

            return true;
        } catch (SQLException e){
            e.printStackTrace();
        } finally {
            connector.closeStatement(st);
        }

        return false;
    }

    @Override
    public boolean delete(int id) {
        PreparedStatement st = null;

        try{
            st = connector.getPreparedStatement(manager.getSqlQueries("deleteProduct"));

            st.setString(1, Integer.toString(id));

            st.executeUpdate();

            return true;
        } catch (SQLException e){
            e.printStackTrace();
        } finally {
            connector.closeStatement(st);
        }

        return false;
    }
}
