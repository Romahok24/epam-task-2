package by.epam.DAO;

public abstract class DAOFactory {
    public static final int MYSQL = 1;

    public abstract ProductDAO getProductDAO();
    public abstract SupplierDAO getSupplierDAO();
    public abstract SoldProductDAO getSoldProductDAO();
    public abstract ProductInBakeryDAO getProductInBakeryDAO();

    public static DAOFactory getDAOFactory(int whichFactory){
        switch (whichFactory){
            case MYSQL: return new MySQLDAOFactory();
            default: return null;
        }
    }
}
