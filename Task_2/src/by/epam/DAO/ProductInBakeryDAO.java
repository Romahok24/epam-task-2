package by.epam.DAO;

import by.epam.ConfigurationManager;
import by.epam.Connector;
import by.epam.subject.ProductInBakery;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ProductInBakeryDAO extends AbstractDAO<ProductInBakery> {
    ConfigurationManager manager = ConfigurationManager.INSTANCE;

    public ProductInBakeryDAO(){
        this.connector = Connector.getInstance();
    }

    @Override
    public boolean create(ProductInBakery entity) {
        PreparedStatement st = null;

        try {
            st = connector.getPreparedStatement(manager.getSqlQueries("addProductInBakery"));

            st.setString(1, Integer.toString(entity.getId()));
            st.setString(2, entity.getName());
            st.setString(3, Integer.toString(entity.getSupplierId()));
            st.setString(4, entity.getDeliverDate().toString());
            st.setString(5, entity.getType());
            st.setString(6, Double.toString(entity.getPrice()));

            st.executeUpdate();

            return true;
        } catch (SQLException e){
            e.printStackTrace();
        } finally {
            connector.closeStatement(st);
        }

        return false;
    }

    @Override
    public List<ProductInBakery> read() {
        Statement st = null;
        List<ProductInBakery> list = new ArrayList<>();

        try{
            st = connector.getStatement();

            ResultSet rs = st.executeQuery(manager.getSqlQueries("readProductsInBakery"));

            while (rs.next()){
                ProductInBakery product = new ProductInBakery();

                product.setId(rs.getInt("id"));
                product.setName(rs.getString("name"));
                product.setSupplierId(rs.getInt("supplierId"));
                product.setDeliverDate(rs.getDate("deliverDate"));
                product.setType(rs.getString("type"));
                product.setPrice(rs.getDouble("price"));

                list.add(product);
            }
        } catch (SQLException e){
            e.printStackTrace();
        } finally {
            connector.closeStatement(st);
        }

        return list;
    }

    @Override
    public boolean update(ProductInBakery entity) {
        PreparedStatement st = null;

        try{
            st = connector.getPreparedStatement(manager.getSqlQueries("updateProductInBakery"));


            st.setString(1, entity.getName());
            st.setString(2, Integer.toString(entity.getSupplierId()));
            st.setString(3, entity.getDeliverDate().toString());
            st.setString(4, entity.getType());
            st.setString(5, Double.toString(entity.getPrice()));
            st.setString(6, Integer.toString(entity.getId()));

            st.executeUpdate();

            return true;
        } catch (SQLException e){
            e.printStackTrace();
        } finally {
            connector.closeStatement(st);
        }

        return false;
    }

    @Override
    public boolean delete(int id) {
        PreparedStatement st = null;

        try{
            st = connector.getPreparedStatement(manager.getSqlQueries("deleteProductInBakery"));

            st.setString(1, Integer.toString(id));

            st.executeUpdate();

            return true;
        } catch (SQLException e){
            e.printStackTrace();
        } finally {
            connector.closeStatement(st);
        }

        return false;
    }
}
