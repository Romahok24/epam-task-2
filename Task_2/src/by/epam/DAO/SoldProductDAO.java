package by.epam.DAO;

import by.epam.ConfigurationManager;
import by.epam.Connector;
import by.epam.subject.SoldProduct;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class SoldProductDAO extends AbstractDAO<SoldProduct> {
    ConfigurationManager manager = ConfigurationManager.INSTANCE;

    public SoldProductDAO(){
        this.connector = Connector.getInstance();
    }

    @Override
    public boolean create(SoldProduct soldProduct) {
        PreparedStatement st = null;

        try{
            st = connector.getPreparedStatement(manager.getSqlQueries("addSoldProduct"));

            st.setString(1, Integer.toString(soldProduct.getId()));
            st.setString(2, soldProduct.getName());
            st.setString(3, Integer.toString(soldProduct.getSupplierId()));
            st.setString(4, soldProduct.getDeliverDate().toString());
            st.setString(5, soldProduct.getType());
            st.setString(6, Double.toString(soldProduct.getPrice()));
            st.setString(7, soldProduct.getDateOfSale().toString());
            st.setString(8, Integer.toString(soldProduct.getCashBoxId()));

            st.executeUpdate();

            return true;
        } catch (SQLException e){
            e.printStackTrace();
        }finally {
            connector.closeStatement(st);
        }

        return false;
    }

    @Override
    public List<SoldProduct> read() {
        List<SoldProduct> products = new ArrayList<>();
        Statement st = null;

        try {
            st = connector.getStatement();
            ResultSet rs = st.executeQuery(manager.getSqlQueries("readSoldProducts"));

            while (rs.next()){
                SoldProduct product = new SoldProduct();
                product.setId(rs.getInt("id"));
                product.setName(rs.getString("name"));
                product.setSupplierId(rs.getInt("supplierId"));
                product.setDeliverDate(rs.getDate("deliverDate"));
                product.setType(rs.getString("type"));
                product.setPrice(rs.getDouble("price"));
                product.setDateOfSale(rs.getDate("dateOfSale"));
                product.setCashBoxId(rs.getInt("cashBoxId"));

                products.add(product);
            }
        } catch (SQLException e){
            e.printStackTrace();
        }finally {
            this.closeStatement(st);
        }
        return products;
    }

    @Override
    public boolean update(SoldProduct entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean delete(int id) {
        throw new UnsupportedOperationException();
    }

    public List<SoldProduct> findByType(String type){
        List<SoldProduct> products = new ArrayList<>();
        PreparedStatement st = null;

        try {
            st = connector.getPreparedStatement(manager.getSqlQueries("selectByType"));
            st.setString(1, type);
            ResultSet rs = st.executeQuery();

            while (rs.next()){
                SoldProduct product = new SoldProduct();
                product.setId(rs.getInt("id"));
                product.setName(rs.getString("name"));
                product.setSupplierId(rs.getInt("supplierId"));
                product.setDeliverDate(rs.getDate("deliverDate"));
                product.setType(rs.getString("type"));
                product.setPrice(rs.getDouble("price"));
                product.setDateOfSale(rs.getDate("dateOfSale"));
                product.setCashBoxId(rs.getInt("cashBoxId"));

                products.add(product);
            }
        } catch (SQLException e){
            e.printStackTrace();
        }finally {
            this.closeStatement(st);
        }
        return products;
    }

    public List<SoldProduct> findByCashbox(int cashboxId){
        List<SoldProduct> products = new ArrayList<>();
        PreparedStatement st = null;

        try {
            st = connector.getPreparedStatement(manager.getSqlQueries("selectByCashbox"));
            st.setString(1, Integer.toString(cashboxId));
            ResultSet rs = st.executeQuery();

            while (rs.next()){
                SoldProduct product = new SoldProduct();
                product.setId(rs.getInt("id"));
                product.setName(rs.getString("name"));
                product.setSupplierId(rs.getInt("supplierId"));
                product.setDeliverDate(rs.getDate("deliverDate"));
                product.setType(rs.getString("type"));
                product.setPrice(rs.getDouble("price"));
                product.setDateOfSale(rs.getDate("dateOfSale"));
                product.setCashBoxId(rs.getInt("cashBoxId"));

                products.add(product);
            }
        } catch (SQLException e){
            e.printStackTrace();
        }finally {
            this.closeStatement(st);
        }
        return products;
    }

    public List<SoldProduct> findByMonth(int month){
        List<SoldProduct> products = new ArrayList<>();
        PreparedStatement st = null;

        try {
            st = connector.getPreparedStatement(manager.getSqlQueries("selectByMonth"));
            st.setString(1, Integer.toString(month));
            ResultSet rs = st.executeQuery();

            while (rs.next()){
                SoldProduct product = new SoldProduct();
                product.setId(rs.getInt("id"));
                product.setName(rs.getString("name"));
                product.setSupplierId(rs.getInt("supplierId"));
                product.setDeliverDate(rs.getDate("deliverDate"));
                product.setType(rs.getString("type"));
                product.setPrice(rs.getDouble("price"));
                product.setDateOfSale(rs.getDate("dateOfSale"));
                product.setCashBoxId(rs.getInt("cashBoxId"));

                products.add(product);
            }
        } catch (SQLException e){
            e.printStackTrace();
        }finally {
            this.closeStatement(st);
        }
        return products;
    }
}
