package by.epam;

import java.util.Locale;
import java.util.ResourceBundle;

public enum ConfigurationManager {
    INSTANCE;

    private ResourceBundle resourceBundle;
    private ResourceBundle queryBundle;
    private ResourceBundle localeBundle;

    private final String resourceName = "property.config";
    private final String sqlQueries = "property.sqlQueries";
    private final String localeString = "property.resources";

    private ConfigurationManager() {
        resourceBundle = ResourceBundle.getBundle(resourceName, Locale.getDefault());
        queryBundle = ResourceBundle.getBundle(sqlQueries, Locale.getDefault());
        localeBundle = ResourceBundle.getBundle(localeString, Locale.getDefault());
    }
    public void changeResource(Locale locale) {
        resourceBundle = ResourceBundle.getBundle(resourceName, locale);
        queryBundle = ResourceBundle.getBundle(sqlQueries, locale);
        localeBundle = ResourceBundle.getBundle(localeString, locale);
    }
    public String getConfig(String key) {
        return resourceBundle.getString(key);
    }

    public String getSqlQueries(String key){
        return queryBundle.getString(key);
    }

    public String getString(String key){
        return localeBundle.getString(key);
    }
}
