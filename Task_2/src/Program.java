import by.epam.ConfigurationManager;
import by.epam.DAO.*;
import by.epam.subject.*;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Locale;

public class Program {
    public static void main(String[] arg){
        DAOFactory mySQL = DAOFactory.getDAOFactory(DAOFactory.MYSQL);
        ConfigurationManager manager = ConfigurationManager.INSTANCE;
        int increment = 1;

        SupplierDAO supplierDAO = mySQL.getSupplierDAO();
        ProductDAO productDAO = mySQL.getProductDAO();
        SoldProductDAO soldProductDAO = mySQL.getSoldProductDAO();
        ProductInBakeryDAO productInBakeryDAO = mySQL.getProductInBakeryDAO();

        List<Product> products = productDAO.read();
        List<Supplier> suppliers = supplierDAO.read();
        List<SoldProduct> soldProducts = soldProductDAO.read();
        List<ProductInBakery> productsInBakery = productInBakeryDAO.read();

        Supplier newSupplier = null;
        // Пример добавления записей в таблицу.
        try {
            System.out.println(new String(manager.getString("exampleCreate").getBytes("ISO-8859-1"), "UTF-8"));
            newSupplier = new Supplier(suppliers.size() + increment, "SweetDream", "12345678");
            supplierDAO.create(newSupplier);

            Product newProduct = new Product(products.size() + increment, "Сказка", newSupplier, "Торт", 8.95);
            productDAO.create(newProduct);

            ProductInBakery newProductInBakery = new ProductInBakery(productsInBakery.size() + increment,
                    newProduct.getName(), newProduct.getSupplier(), newProduct.getType(), newProduct.getPrice());
            System.out.println(productInBakeryDAO.create(newProductInBakery));

            SoldProduct newSoldProduct = new SoldProduct(soldProducts.size() + increment,
                    newProductInBakery.getName(), newProductInBakery.getSupplier(), newProductInBakery.getDeliverDate(),
                    newProductInBakery.getType(), newProductInBakery.getPrice());
            System.out.println(soldProductDAO.create(newSoldProduct));
        } catch (UnsupportedEncodingException e){
            e.printStackTrace();
        }

        products = productDAO.read();
        suppliers = supplierDAO.read();
        soldProducts = soldProductDAO.read();
        productsInBakery = productInBakeryDAO.read();

        // Пример редактирования записи в таблице
        try{
            System.out.println(new String(manager.getString("exampleUpdate").getBytes("ISO-8859-1"), "UTF-8"));
            newSupplier.setPassword("121212");
            System.out.println(supplierDAO.update(newSupplier));
        } catch (UnsupportedEncodingException e){
            e.printStackTrace();
        }

        manager.changeResource(Locale.US);

        // Пример удаления записи в таблице
        System.out.println(manager.getString("exampleDelete"));
        System.out.println(productInBakeryDAO.delete(productsInBakery.size()));

        // Привязка суплаеров и продуктов.
        for(Supplier supplier : suppliers) {
            for(Product product : products){
                if(supplier.getId() == product.getSupplierId()){
                    supplier.addProduct(product);
                    product.setSupplier(supplier);
                }
            }

            // Привязка суплаеров и продуктов в магазине.
            for(ProductInBakery item : productsInBakery){
                if(supplier.getId() == item.getId()){
                    item.setSupplier(supplier);
                    supplier.addProductInBakery(item);
                }
            }

            // Привязка суплаеров и проданных продуктов.
            for(SoldProduct item : soldProducts){
                if(supplier.getId() == item.getSupplierId()){
                    item.setSupplier(supplier);
                    supplier.addSoldProduct(item);
                }
            }

        }

        // Just output.
        System.out.println(manager.getString("getAll"));
        for(Supplier supplier : suppliers) {
            System.out.println(supplier.toString());
        }

        for(SoldProduct soldProduct : soldProducts) {
            System.out.println(soldProduct.toString());
        }

        for(ProductInBakery item : productsInBakery){
            System.out.println(item.toString());
        }

        for(Product product : products){
            System.out.println(product.toString());
        }

        System.out.println(soldProductDAO.findByCashbox(1).size());
        System.out.println(soldProductDAO.findByType("Торт").size());
        System.out.println(soldProductDAO.findByMonth(12).size());

    }
}
